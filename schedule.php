<?php
/*
 * This script must be called from the system every hour (i.e. cron)
 */

require_once('/data/config.php');
require_once('shared.php');

if (DEBUG) botlog("\n\n\n".'[II] Scheduler starting at ' . date('Y-m-d H:i:s') . ' ====================' . "\n\n");

// Get current date in sqlite datetime format
$dateID = date('Y-m-d H:i:s');

// Connect to MySQL DB
$sql = new Sqlite3(DBFILE);
$query = $sql->query("SELECT MessageID,ChatID,DateTime,Text,Author FROM telegram_post WHERE DateTime<='".$dateID."'")
      or error("Can't make the query, SQL error ".$sql->error);

// Post all messages (only text)
while ($row = $query->fetchArray()) {
  info("Publishing message $row[MessageID]");
  /*
  $bot_query = API_URL . API_TOKEN . '/sendMessage?' .
  'chat_id=' . urlencode(API_CHANNEL_ID) .
  '&text=' . urlencode($row['Text']."\n_".$row['Author']."_").
  "&parse_mode=".urlencode("Markdown");
  file_get_contents($bot_query);
  * */
  forwardMessage(API_CHANNEL_ID, $row['ChatID'], $row['MessageID'], FORWARD_SILENT) or
    error("Scheduler couldn't forward the message. I'll try next hour");

}

// Remove just posted messages (and possibly any previous one)
$sql->query("DELETE FROM telegram_post WHERE DateTime<='".$dateID."'");
$sql->close();
?>
