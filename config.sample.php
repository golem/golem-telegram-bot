<?php

  /* Telegram API URL - this must not be changed */
  const API_URL           = 'https://api.telegram.org/bot';
  /* Your API authorization token */
  const API_TOKEN         = '';
  /* Your channel ID */
  const API_CHANNEL_ID    = '@';
  /* Users whose messages are forwarded to mailing list and channel */
  const BOT_ALLOWED_USERS = array ();
  /* Address to forward to */
  const MAIL_TO_ADDR      = 'example@example.com';
  /* Bot email identity */
  const MAIL_FROM_ADDR    = 'example@example.com';
  /* Debug messages on log file */
  const DEBUG             = true;
  /* Be silent when forwarding/writing messages to channel */
  const FORWARD_SILENT    = true;
  /* Log file name */
  const LOGFILE           = 'botlog.log';
  /* Database settings */
  const DBFILE              = '/data/database.sqlite3';
  /* Memcached server connection */
  const MEMCACHED_PORT      = '11211';
  const MEMCACHED_HOST      = 'memcached';

  $EMOJI_THUMBSUP   = mb_convert_encoding('&#x1F44D;', 'UTF-8', 'HTML-ENTITIES');
  $EMOJI_THUMBSDOWN = mb_convert_encoding('&#x1F44E;', 'UTF-8', 'HTML-ENTITIES');
  $EMOJI_CLOCK      = mb_convert_encoding('&#x23F0;', 'UTF-8', 'HTML-ENTITIES');
  $EMOJI_BOT        = mb_convert_encoding('&#x1F916;', 'UTF-8', 'HTML-ENTITIES');
  $EMOJI_MSG        = mb_convert_encoding('&#x1F4E7;', 'UTF-8', 'HTML-ENTITIES');
  $EMOJI_X          = mb_convert_encoding('&#x274C;', 'UTF-8', 'HTML-ENTITIES');
  $EMOJI_TOOLS      = mb_convert_encoding('&#x1F528;', 'UTF-8', 'HTML-ENTITIES');


  // States Flags
  const STATE_IDLE        = 'idle';
  const STATE_WAIT_DATE   = 'wait_date';
  const STATE_WAIT_TIME   = 'wait_time';
  const STATE_MSG_ANSWER  = 'msg_answer';

  // Saved variables in memcached
  const MC_STATUS                 = 'status';
  const MC_FORWARD_MSG            = 'msg_forward';
  const MC_DATE_MSG               = 'msg_date';
  const MC_INLINE_ID              = 'inline_id';
  const MC_FW_EDIT                = 'fw_edit'; // should not be necessary anymore
  const MC_LIST_NUMBER            = 'list_number';
  const MC_DELETE_SCHEDULED_ID    = 'delete_sch_id';

  // buttons costants
  const MSG_YES           = 'y';
  const MSG_NO            = 'n';
  const MSG_SCHEDULE      = 'sch';
  const MSG_ABORT         = 'abort';
  const MSG_DELETE        = 'delete';

  $WELCOME_MESSAGE   = "$EMOJI_BOT GOLEMbot
$EMOJI_MSG Se scrivi un messaggio ti chiedo se inoltrarlo al canale
Supporto anche **<b>grassetto</b>**, __<i>corsivo</i>__ e `<code>codice</code>`
$EMOJI_TOOLS Se scrivi un comando eseguo operazioni.
Comandi disponibili: /help, /list";
?>
