<?php
  require_once('/data/config.php');

  // Base logging function. All logging functions use this.
  // Change this to output everything on stdout, on file, on socket, on whathever you want...
  function botlog($info) {
    error_log($info);
  }

  // Unrecoverable error, program must be terminated
  function error($message) {
    if (DEBUG) botlog("[EE] $message");
    if (DEBUG) botlog('[II] BOT request died');
    die();
  }

  // Uncommon event
  function warning($message) {
    if (DEBUG) botlog("[WW] $message");
  }

  // Routine debugging informations
  function info($message) {
    if (DEBUG) botlog("[II] $message");
  }

// Interpreta un comando inviato al bot - mettere null o simili invece di -1
  function getCommand($messaggio, $NoOffset = true) {
    if (!isset($messaggio->entities))
      return null;
    foreach ($messaggio->entities as $entity) {
      if ($entity->type == "bot_command") {
        if ($NoOffset == true and $entity->offset != 0)
          return null;
        //~ if (DEBUG) botlog("[II] Comando: ".$entity->offset. " ".$messaggio->text);
        //return substr($messaggio->text, $entity->offset, $entity->length);
        $ret['command'] = substr($messaggio->text, $entity->offset, $entity->length);
        $ret['options'] = substr($messaggio->text, $entity->length+$entity->offset+1);
        info("Command: $ret[command]");
        return $ret;
      }
    }
    return null;
  }

  /**** Telegram query functions ****/

  // return a message object, null if is error
  function sendMessage($chat_id, $text, $parse_mode = null,
                       $disable_web_preview = false,
                       $disable_notification = false,
                       $reply_to_message_id = null,
                       $reply_markup = null) {
    $query = API_URL.API_TOKEN."/sendMessage?chat_id=".$chat_id.
          "&text=".urlencode($text);

    if ($parse_mode != null)   $query .= "&parse_mode=$parse_mode";
    if ($reply_markup != null) $query .= "&reply_markup=$reply_markup";
    if ($disable_web_preview != false) $query .= "&disable_web_preview=$disable_web_preview";
    if ($disable_notification != false) $query .= "&disable_notification=$disable_notification";
    if ($reply_to_message_id != null) $query .= "&reply_to_message_id=$reply_to_message_id";
    if ($reply_markup != null) $query .= "&reply_markup=$reply_markup";

    $answer = file_get_contents($query);
    if ($answer === false) warning("Something is wrong in sendMessage - chat_id: $chat_id - text: $text");
    return $answer;
  }

  //
  function forwardMessage($chat_id, $from_chat_id,
                       $message_id,
                       $disable_notification = null) {

    $query = API_URL.API_TOKEN."/forwardMessage?" .
            "chat_id=".urlencode($chat_id) .
            "&from_chat_id=" . urlencode($from_chat_id) .
            "&message_id=" . urlencode($message_id);

    if ($disable_notification != null) $query .= "&disable_notification=$disable_notification";

    $answer = file_get_contents($query);
    if ($answer === false) {
      warning("Something is wrong in forwardMessage - chat_id: $chat_id - message_id: $message_id");
      warning($answer);
    }
    return $answer;
  }

  // omitted "inline parameters", for inline bot funtions (not used here)
  function editMessageText($chat_id, $message_id, $text,
                    $parse_mode = null,
                    $disable_web_page_preview = false,
                    $reply_markup = null) {

    $query = API_URL . API_TOKEN . "/editMessageText?" .
            "chat_id=".urlencode($chat_id) .
            "&message_id=" . urlencode($message_id).
            "&text=" . urlencode($text);

    if ($parse_mode != null) $query .= "&parse_mode=$parse_mode";
    if ($disable_web_page_preview != false) $query .= "&disable_web_page_preview=$disable_web_page_preview";
    if ($reply_markup != null) $query .= "&reply_markup=$reply_markup";

    $answer = file_get_contents($query);
    if ($answer === false) warning("Something is wrong in editMessageText - chat_id: $chat_id - message_id: $message_id");
    info("edit message answer: $answer");
    return $answer;
  }

  //
  function deleteMessage($chat_id, $message_id) {

    $query = API_URL . API_TOKEN . "/deleteMessage?" .
            "chat_id=".urlencode($chat_id) .
            "&message_id=" . urlencode($message_id);

    $answer = file_get_contents($query);
    if ($answer === false) warning("Something is wrong in deleteMessage - chat_id: $chat_id - message_id: $message_id");
    return $answer;
  }

  function sendPhoto ($chat_id, $photo,
                      $caption = null,
                      $reply_markup = null,
                      $disable_notification = false,
                      $reply_to_message_id = null  ) {

    $query = API_URL . API_TOKEN . "/sendPhoto?" .
             "chat_id=".urlencode($chat_id) .
             "&photo=" . urlencode($photo);

    $query .= "&disable_notification=$disable_notification";
    if ($reply_to_message_id != null) $query .= "&reply_to_message_id=$disable_web_page_preview";
    if ($reply_markup != null) $query .= "&reply_markup=$reply_markup";
    if ($caption != null) $query .= "&caption=$caption";

    $answer = file_get_contents($query);
  }

  // Makes the calendar inline keyboard
  function getCalendarTab ($month, $year) {
      info('getting calendar...');
    $kbd = new InlineKbd;

    $days = array("L", "M", "M", "G", "V", "S", "D");

    for ($i = 0; $i < 7; $i++)
      $kbd->insertItem("$days[$i]", "null");
    $kbd->pushLine();

    $daysInMonth = cal_days_in_month (CAL_GREGORIAN , $month , $year);
    $date = $year."-".$month;
    $isCurrentMonth = date('nY') == $month.$year;
    $startingDay = $isCurrentMonth ? date('j') : 1;
    info("[II] First day: ".$date.'-'.$startingDay."\n");
    $firstDay    = date("N", strtotime($date.'-'.$startingDay));

    // blank buttons
    for ($i = 1; $i < $firstDay; $i++)
      $kbd->insertItem(" ", "null");

    for ($numDay = $startingDay; $numDay <= $daysInMonth; $numDay++, $i++) {
      $kbd->insertItem("$numDay", "$numDay");

      if ($i >= 7) {
        $kbd->pushLine();
        $i = 0;
      }
    }

    // empty buttons
    if ($i != 0) {
      for (; $i > 1 and $i <= 7; $i++)
        $kbd->insertItem(" ", "null");
      $kbd->pushLine();
    }


    if ($isCurrentMonth)
      $kbd->insertItem(" ", "null");
    else
      $kbd->insertItem("<", "<");
    $kbd->insertItem("$EMOJI_X Annulla",MSG_ABORT);
    $kbd->insertItem(">", ">");

    $kbd->pushLine();

    return $kbd->getKeyboard();
  }


  // Function used to get an archived message
  // and make an appropriate keyboard

  // -- I dont' like so much --
  function navigateMsgArchive ($msgNumber, $mc, $chatID) {
      $sql = new Sqlite3(DBFILE);
    $numbers = $sql->query("SELECT COUNT(*) FROM telegram_post");
    $numbers = $numbers->fetchArray()[0];
    if ($numbers == 0) {
      return array( "text" => "Nessun messaggio programmato",
                    "kbd"  => null);
      $sql->close();
      //~ $query = API_URL.API_TOKEN."/sendmessage?chat_id=".($chatID).
        //~ "&text=Nessun messaggio programmato";
      //~ $answer = file_get_contents($query);
      //~ die();
    }
    else {
      $query = $sql->query("SELECT DateTime as DateTimeFormat,ChatID, MessageID,Text,Author,ID FROM telegram_post ORDER BY DateTime ASC LIMIT 1 OFFSET $msgNumber") or error("Can't make the query, SQL error ".$sql->error);

      $row = $query->fetchArray();

      $mc->set($chatID.MC_DELETE_SCHEDULED_ID, $row['ID'])
      or $mc->replace($chatID.MC_DELETE_SCHEDULED_ID, $row['ID']);

      $sql->close();

      $kbd = new InlineKbd;
      $kbd->insertItem($row['DateTimeFormat'], "null");
      $kbd->pushLine();
      if ($msgNumber == 0)
        $kbd->insertItem(" ","null");
      else
        $kbd->insertItem("<","<");
      $kbd->insertItem("Rimuovi", MSG_DELETE);
      if ($numbers - $msgNumber > 1)
        $kbd->insertItem(">", ">");
      else
        $kbd->insertItem(" ","null");
      $kbd->pushLine();
      $kbd = $kbd->getKeyboard();


      if ($row['Text'] != ''){
        $text="$row[Text]\n<i>$row[Author]</i>"; // NOME CANALE ".API_CHANNEL_ID."?";
      }
      else
        $text="Non testo puro";

      return array( "text" => $text,
                    "kbd"  => $kbd);
    }
  }

  function not_authorized($chatID) {
    $query = API_URL . API_TOKEN . '/sendMessage?' .
      'chat_id=' . urlencode($chatID) .
      "&text=".urlencode("Questo bot è solo per il Consiglio Direttivo.\nForse cercavi @golem_empoli (canale) oppure https://golem.linux.it/ (sito)");

    file_get_contents($query);

    die();
  }

  function wrong_action($chatID) {
    $query = API_URL . API_TOKEN . '/sendMessage?' .
      'chat_id=' . urlencode($chatID) .
      "&text=Azione non corretta";

    file_get_contents($query);

    die();
  }


  class InlineKbd
  {
      private $keyboard = array();
      private $currentLine = array();

      public function pushLine() {
        $this->keyboard[] = $this->currentLine;
        $this->currentLine = array();
      }
      public function insertItem($text, $callback_data) {
        $this->currentLine[] = array(
            "text" => $text,
            "callback_data" => $callback_data
            );
      }
      public function getKeyboard() {
        $kbd = array("inline_keyboard" => $this->keyboard);
        return  json_encode($kbd);
      }
  }
?>
